import { AppRoutingModule } from './/app-routing.module';
import { AppComponent } from './app.component';
import { DisplayComponent } from './display/display.component';
import { HeaderComponent } from './header/header.component';
import { ModifRecipeComponent } from './modif-recipe/modif-recipe.component';
import { NewRecipeComponent } from './new-recipe/new-recipe.component';
import { RecipeDisplayComponent } from './recipe-display/recipe-display.component';
import { RecipeService } from './recipe.service';
import { SearchDisplayComponent } from './search-display/search-display.component';
import { SearchFieldComponent } from './search-field/search-field.component';
import { SearchService } from './search.service';
import { SearchComponent } from './search/search.component';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NouisliderModule } from 'ng2-nouislider';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SearchFieldComponent,
    NewRecipeComponent,
    DisplayComponent,
    RecipeDisplayComponent,
    SearchComponent,
    SearchDisplayComponent,
    ModifRecipeComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule, NouisliderModule, HttpClientModule],
  providers: [RecipeService, SearchService],
  bootstrap: [AppComponent],
})
export class AppModule {}
