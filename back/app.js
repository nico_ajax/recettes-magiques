let express = require("express"),
  app = express(),
  recipes = require("./routes/recipes"),
  mongoose = require("mongoose"),
  search = require("./routes/search"),
  ip = require("ip");

console.log(ip.address());

mongoose.Promise = global.Promise;
mongoose
  .connect("mongodb://database/recipes")
  .then(() => console.log("connection succesful"))
  .catch(err => console.error(err));

app.get("", function(req, res, next) {
  res.send("Hello");
});
app.use(function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "http://192.168.99.100:4200");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});
app.use(express.json());

app.use("/search", search);
app.use("/recipes", recipes);

app.listen(8080);
module.exports = app;
