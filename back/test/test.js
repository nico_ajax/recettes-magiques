let expect = require("chai").expect;
let request = require("supertest");
let app = require("../app");
let agent = request.agent(app);

// get /
describe("GET /", function() {
  it("returns users as JSON", function(done) {
    agent.get("/").expect(200);
  });
});
