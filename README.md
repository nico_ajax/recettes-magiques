## Step 1

Install Prettier in VSCode extention manager.

## Step 2

Add custom parameters to VSCode options (CMD/Ctrl + ,)

```
    "editor.formatOnSave": true,
    "typescript.extension.sortImports.sortOnSave": true,  //Only if you have installed the sort-imports extension
    "typescript.extension.sortImports.sortMethod": "path"  //Only if you have installed the sort-imports extension
```

## Step 3

Create custom .prettierrc settings file in project.
Add content to settings file :

```
{
  "semi": true,
  "singleQuote": true,
  "trailingComma": "all",
  "printWidth": 100
}
```

## You're good to go !
